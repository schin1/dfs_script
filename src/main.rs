use anyhow::Result;

use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::io::Read;
use std::io::SeekFrom;
use std::str;

fn main() -> Result<()> {
    // open and print all in folder
    let dir = std::env::args()
        .nth(1)
        .unwrap_or("/tmp/prometheus_test/".to_string());
    print_dir_dfs(dir)?;

    Ok(())
}

fn print_dir_dfs(dir: String) -> Result<()> {
    let metric_paths: Vec<String> = fs::read_dir(dir)?
        .into_iter()
        .filter_map(|file| file.ok())
        .filter(|file| file.metadata().unwrap().is_file())
        .map(|file| file.path().display().to_string())
        .collect();

    metric_paths.iter().for_each(|path| {
        open_dfs(path.to_string());
    });

    Ok(())
}

fn open_dfs(path: String) -> Result<()> {
    println!("{}", &path);
    let mut f = File::open(path)?;
    let mut used_buf = [0; 4];
    f.read(&mut used_buf)?;
    let used: i32 = i32::from_ne_bytes(used_buf[0..4].try_into().unwrap());

    println!("Used bytes: {}", used);

    f.seek(SeekFrom::Current(4))?; // skip padding bytes

    let mut cursor = 8;
    while cursor < used {
        let mut pad_buffer = [0; 4];
        f.read(&mut pad_buffer[..])?;

        let pad: i32 = i32::from_ne_bytes(pad_buffer[0..4].try_into().unwrap());
        let mut buf = vec![0; pad.try_into().unwrap()];
        f.read(&mut buf)?;

        let s = match str::from_utf8(&buf) {
            Ok(v) => v,
            Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
        };

        let mut value_buffer = [0; 8];
        f.read(&mut value_buffer[..])?;

        let mut time_buffer = [0; 8];
        f.read(&mut time_buffer[..])?;

        // could return a struct to be printed?
        println!(
            "{{{}}} {} {}",
            s.trim(),
            f64::from_ne_bytes(value_buffer[0..8].try_into().unwrap()),
            f64::from_ne_bytes(time_buffer[0..8].try_into().unwrap())
        );

        cursor += 4 + pad + 8 + 8;
    }
    println!();
    Ok(())
}
